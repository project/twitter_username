<?php

namespace Drupal\twitter_username\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\twitter_username\Plugin\Field\FieldType\TwitterUsername;

/**
 * Plugin implementation of the 'twitter_username_textfield' widget.
 *
 * @FieldWidget(
 *   id = "twitter_username_textfield",
 *   label = @Translation("Twitter username textfield"),
 *   description = @Translation("Twitter username specific textfield."),
 *   field_types = {
 *     "twitter_username"
 *   },
 * )
 */
class TwitterUsernameDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['value'] = $element + [
      '#type' => 'textfield',
      '#default_value' => $items[$delta]->value ?? NULL,
      '#size' => 60,
      '#placeholder' => $this->getSetting('placeholder'),
      '#maxlength' => TwitterUsername::TWITTER_USERNAME_MAX_LENGTH,
      '#field_prefix' => "@",
    ];

    return $element;
  }

}
