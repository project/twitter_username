## INTRODUCTION ##

Author and maintainer: **Opi**
 * Drupal: https://www.drupal.org/u/opi

Maintainer: **Andrii Sakhaniuk**
 * Drupal: https://www.drupal.org/u/nnevill

This module provides a simple field for handling Twitter username,
which can be added to content types for validating and storing a
Twitter username.
This module only checks the syntax, not that the username really exists.

## INSTALLATION ##

See https://www.drupal.org/documentation/install/modules-themes/modules-8
for instructions on how to install or update Drupal modules.

## CONFIGURATION ##

* Add new field with type "Twitter username".
* Configure widget and formatter.
* Create new node to check if it works.

### PERMISSIONS ###

There are no specific permissions created for this module.

### Credits ###

Thanks to Niklas Fiekas (Drupal user #1089248) for his patience and his
advices.
