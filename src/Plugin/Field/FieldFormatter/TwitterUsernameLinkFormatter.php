<?php

namespace Drupal\twitter_username\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\twitter_username\Plugin\Field\FieldType\TwitterUsername;

/**
 * Plugin implementation of the 'twitter_username_link' formatter.
 *
 * @FieldFormatter(
 *   id = "twitter_username_link",
 *   label = @Translation("Link"),
 *   description = @Translation("Allows to display twitter username as a link."),
 *   field_types = {
 *     "twitter_username",
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class TwitterUsernameLinkFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'link_type' => 'default',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $link_type = $this->getSetting('link_type');

    foreach ($items as $delta => $item) {
      $twitter_path = $item->value;

      if ($link_type) {
        $twitter_path = "{$twitter_path}/{$link_type}";
      }
      $elements[$delta] = [
        '#type' => 'link',
        '#title' => '@' . $item->value,
        '#url' => Url::fromUri(TwitterUsername::TWITTER_USERNAME_TWITTER_URL . '@' . $twitter_path),
        '#langcode' => $item->getLangcode(),
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $link_type = $this->getSetting('link_type');

    $summary = [$this->t('Link type: @link_type', ['@link_type' => $link_type])];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $element['link_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Link type'),
      '#description' => $this->t('Select the link type.'),
      '#default_value' => $this->getSetting('link_type'),
      '#options' => [
        '' => t('Default'),
        'with_replies' => t('Replies'),
        'media' => t('Media'),
        'likes' => t('Likes'),
      ],
    ];
    return $element;
  }

}
